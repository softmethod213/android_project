package com.example.Photo_App;

import android.content.Context;

import com.example.Photo_App.objects.Album;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

public class Serialization implements Serializable {
    public static String file = "photodata.ser";
    public ArrayList<Album> albums;

    public void save(Context context){
        try{
            FileOutputStream fos = context.openFileOutput(file, Context.MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(fos);

            oos.writeObject(this);

            fos.close();
            oos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Serialization load(Context context){
        Serialization object = null;

        try{
            FileInputStream fis = context.openFileInput(file);
            ObjectInputStream ois = new ObjectInputStream(fis);

            object =(Serialization)ois.readObject();

            fis.close();
            ois.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return object;
    }

}
