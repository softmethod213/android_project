package com.example.Photo_App;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.Photo_App.objects.Photo;
import com.example.Photo_App.objects.Tag;

import java.util.ArrayList;

public class ResultsView extends AppCompatActivity{
    private ImageView fullimage;
    private Button next;
    private Button previous;
    private Button addTag;
    private Button deleteTag;
    private Spinner tagSpinner;

    private int pos = 0;
    private Photo currentPhoto;
    private ArrayAdapter<Tag> tagAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.photo_view_main);

        pos = getIntent().getExtras().getInt(Search.RESULTS_PHOTO_POS);
        currentPhoto = MainActivity.searchList.get(pos);

        fullimage = findViewById(R.id.full_photo);
        next = findViewById(R.id.next_photo);
        previous = findViewById(R.id.previous_photo);
        addTag = findViewById(R.id.add_tag);
        deleteTag = findViewById(R.id.delete_tag);

        tagSpinner = findViewById(R.id.tagSpinner);

        tagAdapter = new ArrayAdapter<>(this, R.layout.spinner_text, currentPhoto.tagArrayList);
        tagSpinner.setAdapter(tagAdapter);

        fullimage.setImageBitmap(currentPhoto.getBmap());

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    Photo nextPhoto = MainActivity.searchList.get(pos + 1);
                    fullimage.setImageBitmap(nextPhoto.getBmap());
                    pos++;
                    currentPhoto = nextPhoto;
                    tagAdapter = new ArrayAdapter<>(getApplicationContext(), R.layout.spinner_text, currentPhoto.tagArrayList);
                    tagSpinner.setAdapter(tagAdapter);
                }
                catch (IndexOutOfBoundsException e){
                    e.printStackTrace();
                }
            }
        });

        previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    Photo previousPhoto = MainActivity.searchList.get(pos - 1);
                    fullimage.setImageBitmap(previousPhoto.getBmap());
                    pos--;
                    currentPhoto = previousPhoto;
                    if(currentPhoto.tagArrayList == null){
                        currentPhoto.tagArrayList = new ArrayList<>();
                    }
                    tagAdapter = new ArrayAdapter<>(getApplicationContext(), R.layout.spinner_text, currentPhoto.tagArrayList);
                    tagSpinner.setAdapter(tagAdapter);
                }
                catch (IndexOutOfBoundsException e){
                    e.printStackTrace();
                }
            }
        });

        addTag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),"Disabled for Result Photo", Toast.LENGTH_LONG).show();
            }
        });

        deleteTag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),"Disabled for Result Photo", Toast.LENGTH_LONG).show();
            }
        });

    }
}
