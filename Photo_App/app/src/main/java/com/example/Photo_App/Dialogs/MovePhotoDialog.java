package com.example.Photo_App.Dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.example.Photo_App.MainActivity;
import com.example.Photo_App.Photo_Main;
import com.example.Photo_App.R;
import com.example.Photo_App.objects.Album;

public class MovePhotoDialog extends AppCompatDialogFragment {

    private Spinner spinner;
    public PhotoDialogListener listener;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final int pos = getArguments().getInt(Photo_Main.PHOTO_POS);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.photo_mover, null);
        spinner = view.findViewById(R.id.spinner);
        ArrayAdapter<Album> spinnerAdapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, MainActivity.photoAlbum.albums);
        spinner.setAdapter(spinnerAdapter);

        builder.setView(view)
                .setTitle("Move to Album")
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        return;
                    }
                })
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Album album =(Album)spinner.getSelectedItem();
                        listener.movePhototoAlbum(album, pos);
                    }
                });
        return builder.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (PhotoDialogListener)context;
    }

    public interface PhotoDialogListener{
        void movePhototoAlbum(Album album, int pos);
    }
}
