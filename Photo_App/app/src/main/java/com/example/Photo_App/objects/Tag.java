package com.example.Photo_App.objects;

import java.io.Serializable;

public class Tag implements Serializable {
    public static final long serialVersionUID = 7056484541963027732L;
    private String type;
    private String name;

    public Tag(String type, String name){
        this.type = type;
        this.name = name;
    }

    public String getType(){
        return this.type;
    }

    public String getName(){
        return this.name;
    }

    @Override
    public String toString() {
        return this.name.concat("@").concat(this.type);
    }
}
