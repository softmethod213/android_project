package com.example.Photo_App;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.Photo_App.Dialogs.AddTagDialog;
import com.example.Photo_App.Dialogs.ErrorDialog;
import com.example.Photo_App.objects.Photo;
import com.example.Photo_App.objects.Tag;

import java.util.ArrayList;

public class Photo_View_Main extends AppCompatActivity implements AddTagDialog.DialogListener {
    private ImageView fullimage;
    private Button next;
    private Button previous;
    private Button addTag;
    private Button deleteTag;
    private Spinner tagSpinner;

    private int pos = 0;
    private Photo currentPhoto;
    private ArrayAdapter<Tag> tagAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.photo_view_main);

        pos = getIntent().getExtras().getInt(Photo_Main.PHOTO_TAG_POS);
        currentPhoto = Photo_Main.photoList.get(pos);
        if(currentPhoto.tagArrayList == null){
            currentPhoto.tagArrayList = new ArrayList<>();
        }

        fullimage = findViewById(R.id.full_photo);
        next = findViewById(R.id.next_photo);
        previous = findViewById(R.id.previous_photo);
        addTag = findViewById(R.id.add_tag);
        deleteTag = findViewById(R.id.delete_tag);

        tagSpinner = findViewById(R.id.tagSpinner);

        tagAdapter = new ArrayAdapter<>(this, R.layout.spinner_text, currentPhoto.tagArrayList);
        tagSpinner.setAdapter(tagAdapter);

        fullimage.setImageBitmap(currentPhoto.getBmap());

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    Photo nextPhoto = Photo_Main.photoList.get(pos + 1);
                    fullimage.setImageBitmap(nextPhoto.getBmap());
                    pos++;
                    currentPhoto = nextPhoto;
                    if(currentPhoto.tagArrayList == null){
                        currentPhoto.tagArrayList = new ArrayList<>();
                    }
                    tagAdapter = new ArrayAdapter<>(getApplicationContext(), R.layout.spinner_text, currentPhoto.tagArrayList);
                    tagSpinner.setAdapter(tagAdapter);
                }
                catch (IndexOutOfBoundsException e){
                    e.printStackTrace();
                }
            }
        });

        previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    Photo previousPhoto = Photo_Main.photoList.get(pos - 1);
                    fullimage.setImageBitmap(previousPhoto.getBmap());
                    pos--;
                    currentPhoto = previousPhoto;
                    if(currentPhoto.tagArrayList == null){
                        currentPhoto.tagArrayList = new ArrayList<>();
                    }
                    tagAdapter = new ArrayAdapter<>(getApplicationContext(), R.layout.spinner_text, currentPhoto.tagArrayList);
                    tagSpinner.setAdapter(tagAdapter);
                }
                catch (IndexOutOfBoundsException e){
                    e.printStackTrace();
                }
            }
        });

        addTag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddTagDialog ad = new AddTagDialog();
                ad.show(getSupportFragmentManager(), "Add Tag");
            }
        });

        deleteTag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Tag currentTag =(Tag)tagSpinner.getSelectedItem();
                currentPhoto.tagArrayList.remove(currentTag);
                tagAdapter.notifyDataSetChanged();
                MainActivity.photoAlbum.save(getApplicationContext());
                Toast.makeText(getApplicationContext(), "Removed", Toast.LENGTH_LONG).show();
            }
        });

    }

    @Override
    public void applyTag(String tagType, String name) {
        if(name.isEmpty()){
            ErrorDialog error = new ErrorDialog();
            error.show(getSupportFragmentManager(), "ERROR");
            return;
        }
        Tag newTag = new Tag(tagType, name);
        this.currentPhoto.tagArrayList.add(newTag);
        tagAdapter.notifyDataSetChanged();
        MainActivity.photoAlbum.save(getApplicationContext());
    }
}
