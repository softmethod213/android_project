package com.example.Photo_App.objects;

import java.io.Serializable;
import java.util.ArrayList;

public class Album implements Serializable {
    public static final long serialVersionUID = -157857587975964645L;
    public ArrayList<Photo> photoArrayList;
    private String name;
    private int count;

    public Album(String name){
        this.name = name;
        photoArrayList = new ArrayList<>();
    }

    public String getName(){
        return this.name;
    }

    public int getCount(){ return this.count; }

    public void setName(String name){ this.name = name; }

    public void setCount(int count){ this.count = count; }

    @Override
    public String toString() {
        return this.getName();
    }
}
