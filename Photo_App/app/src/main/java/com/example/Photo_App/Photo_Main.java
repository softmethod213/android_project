package com.example.Photo_App;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.Photo_App.Dialogs.MovePhotoDialog;
import com.example.Photo_App.objects.Album;
import com.example.Photo_App.objects.Photo;

import java.util.ArrayList;

import static com.example.Photo_App.MainActivity.photoAlbum;

public class Photo_Main extends AppCompatActivity implements MovePhotoDialog.PhotoDialogListener {
    public final static int GALLERY_PHOTO = 1;
    public final static String PHOTO_POS = "PHOTO_POS";
    public final static String PHOTO_TAG_POS = "PHOTO_TAG_POS";

    public static ArrayList<Photo> photoList;
    private PhotoAdapter photoArrayAdapter;
    private GridView photoGV;
    private ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        photoList = photoAlbum.albums.get(getIntent().getExtras().getInt(MainActivity.PHOTO_POS)).photoArrayList;
        if(photoList == null){
            photoList = new ArrayList<>();
        }

        photoGV = findViewById(R.id.photoGridView);
        photoArrayAdapter = new PhotoAdapter();
        photoGV.setAdapter(photoArrayAdapter);

        photoGV.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                showPhotoIntent(position);
            }
        });
        registerForContextMenu(photoGV);
    }

    private void showPhotoIntent(int pos){
        Bundle tags = new Bundle();
        tags.putInt(PHOTO_TAG_POS, pos);
        Intent photoView = new Intent(this, Photo_View_Main.class);
        photoView.putExtras(tags);
        startActivity(photoView);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.menu_photo_gv, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();

        switch (item.getItemId()){
            case R.id.move_photo:
                MovePhotoDialog move = new MovePhotoDialog();
                Bundle photo_pos = new Bundle();
                photo_pos.putInt(Photo_Main.PHOTO_POS, info.position);
                move.setArguments(photo_pos);
                move.show(getSupportFragmentManager(), "Move Photo");
                return true;
            case R.id.delete_photo:
                photoList.remove(info.position);
                photoArrayAdapter.notifyDataSetChanged();
                photoAlbum.save(getApplicationContext());
                Toast.makeText(this, "Removed", Toast.LENGTH_LONG).show();
                return true;
        }
        return super.onContextItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.photo_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.add_photo){
            startActivityForResult(new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images .Media.INTERNAL_CONTENT_URI), GALLERY_PHOTO);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == GALLERY_PHOTO && resultCode == Activity.RESULT_OK){
            Uri selectedImage = data.getData();

            image = new ImageView(this);
            image.setImageURI(selectedImage);

            BitmapDrawable drawable = (BitmapDrawable)image.getDrawable();
            Bitmap photo = drawable.getBitmap();

            Photo newPhoto = new Photo("New Photo", photo);

            photoList.add(newPhoto);
            photoArrayAdapter.notifyDataSetChanged();
            photoAlbum.save(getApplicationContext());

        }
    }

    @Override
    public void movePhototoAlbum(Album album, int pos) {
       if(album.photoArrayList == null){
           album.photoArrayList = new ArrayList<>();
       }

       album.photoArrayList.add(photoList.get(pos));
       this.photoList.remove(pos);
       photoAlbum.save(getApplicationContext());
       photoArrayAdapter.notifyDataSetChanged();

       Toast.makeText(this, "Moved to ".concat(album.getName()), Toast.LENGTH_LONG).show();
    }

    private class PhotoAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return photoList.size();
        }

        @Override
        public Object getItem(int position) { return photoList.get(position); }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = getLayoutInflater().inflate(R.layout.photo, parent, false);
            image = convertView.findViewById(R.id.photo);

            image.setImageBitmap(photoList.get(position).getBmap());

            return convertView;
        }
    }

}
