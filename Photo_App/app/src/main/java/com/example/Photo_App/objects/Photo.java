package com.example.Photo_App.objects;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

public class Photo implements Serializable {
    public static final long serialVersionUID = -5396539761291086152L;
    public ArrayList<Tag> tagArrayList;
    private String caption;
    private transient Bitmap bmap;

    public Photo(String caption, Bitmap bmap){
        this.caption = caption;
        this.bmap = bmap;
        this.tagArrayList = new ArrayList<>();
    }

    public String getCaption(){
        return this.caption;
    }

    public Bitmap getBmap(){ return this.bmap; }

    @Override
    public String toString() {
        return this.caption;
    }

    private void readObject(ObjectInputStream ois) throws IOException, ClassNotFoundException {
        ois.defaultReadObject();
        int b;
        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        while((b = ois.read()) != -1)
            byteStream.write(b);
        byte bitmapBytes[] = byteStream.toByteArray();
        bmap = BitmapFactory.decodeByteArray(bitmapBytes, 0, bitmapBytes.length);
    }

    private void writeObject(ObjectOutputStream oos) throws IOException {
        oos.defaultWriteObject();
        if(bmap != null){
            ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
            bmap.compress(Bitmap.CompressFormat.PNG, 100, byteStream);
            byte bitmapBytes[] = byteStream.toByteArray();
            oos.write(bitmapBytes);
        }
    }

}
