package com.example.Photo_App.Dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.Photo_App.R;

public class AddTagDialog extends AppCompatDialogFragment {

    private EditText tagDescription;
    private Spinner tagTypes;
    public DialogListener listener;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.add_tag_main, null);
        tagDescription = view.findViewById(R.id.tag_description);
        tagTypes = view.findViewById(R.id.tag_types);

        builder.setView(view)
                .setTitle("Add New Tag")
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        listener.applyTag(tagTypes.getSelectedItem().toString(), tagDescription.getText().toString().trim());
                    }
                });
        return builder.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (DialogListener) context;
    }

    public interface DialogListener{
        void applyTag(String tagType, String name);
    }
}
