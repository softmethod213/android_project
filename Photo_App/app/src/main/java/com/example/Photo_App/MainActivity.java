package com.example.Photo_App;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.Photo_App.Dialogs.CreateAlbumDialog;
import com.example.Photo_App.Dialogs.EditAlbumDialog;
import com.example.Photo_App.Dialogs.ErrorDialog;
import com.example.Photo_App.Dialogs.IncorrectParseDialog;
import com.example.Photo_App.Dialogs.SearchDialog;
import com.example.Photo_App.objects.Album;
import com.example.Photo_App.objects.Photo;
import com.example.Photo_App.objects.Tag;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements CreateAlbumDialog.AlbumDialogListener, EditAlbumDialog.AlbumDialogListener, SearchDialog.SearchListener{

    public final static String PHOTO_POS = "PHOTO_POS";
    public final static String BUNDLE_POS = "BUNDLE_POS";
    public final static String PARSE_INFO = "PARSE_INFO";
    public final static String SEARCH_LIST = "SEARCH_LIST";

    public static ArrayList<Photo> searchList;

    public static Serialization photoAlbum;
    private ArrayAdapter<Album> albumAdapter;
    private ListView albumLV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        photoAlbum = Serialization.load(this);

        if(photoAlbum == null){
            photoAlbum = new Serialization();
            photoAlbum.albums = new ArrayList<>();
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        albumAdapter = new ArrayAdapter<>(this, R.layout.album, photoAlbum.albums);
        albumLV = findViewById(R.id.albumListView);
        albumLV.setAdapter(albumAdapter);

        albumLV.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                showPhotoList(position);
            }
        });
        registerForContextMenu(albumLV);
    }

    private void showPhotoList(int pos){
        Bundle photoBundle = new Bundle();
        photoBundle.putInt(PHOTO_POS, pos);

        Intent photoIntent = new Intent(this, Photo_Main.class);
        photoIntent.putExtras(photoBundle);
        startActivity(photoIntent);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.menu_album_lv, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();

        switch (item.getItemId()) {
            case R.id.edit_album:
                EditAlbumDialog ad = new EditAlbumDialog();

                Bundle pos = new Bundle();
                pos.putInt(BUNDLE_POS, info.position);
                ad.setArguments(pos);

                ad.show(getSupportFragmentManager(), "Edit Album Name");
                Toast.makeText(MainActivity.this, "Edited", Toast.LENGTH_LONG).show();
                return true;

            case R.id.delete_album:
                photoAlbum.albums.remove(info.position);
                albumAdapter.notifyDataSetChanged();
                Toast.makeText(MainActivity.this, "Deleted", Toast.LENGTH_LONG).show();
                photoAlbum.save(getApplicationContext());
                return true;

        }
        return super.onContextItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.create_new_album){
            createNewAlbum();
            return true;
        }
        else if(id == R.id.search_tags){
            SearchDialog sd = new SearchDialog();
            sd.show(getSupportFragmentManager(), "Search");
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void createNewAlbum(){
        CreateAlbumDialog ad = new CreateAlbumDialog();
        ad.show(getSupportFragmentManager(), "Add New Album");
    }

    @Override
    public void applyAlbumName(String name) {
        if(name.isEmpty()){
            ErrorDialog error = new ErrorDialog();
            error.show(getSupportFragmentManager(), "ERROR");
            return;
        }
        Album newAlbum = new Album(name);
        photoAlbum.albums.add(newAlbum);
        albumAdapter.notifyDataSetChanged();

        photoAlbum.save(getApplicationContext());
    }

    @Override
    public void editAlbumName(String name, int pos) {
        if(name.isEmpty()){
            ErrorDialog error = new ErrorDialog();
            error.show(getSupportFragmentManager(), "ERROR");
            return;
        }
        photoAlbum.albums.get(pos).setName(name);
        albumAdapter.notifyDataSetChanged();

        photoAlbum.save(getApplicationContext());
    }

    @Override
    public void searchListener(String name) {
        if(name.isEmpty()){
            ErrorDialog error = new ErrorDialog();
            error.show(getSupportFragmentManager(), "ERROR");
            return;
        }
        searchList = new ArrayList<>();
        String[] parts = name.split(" ");

        try{
            if(parts.length == 1){
                String[] singleTag = parts[0].split("=");
                String tagType = singleTag[0].toLowerCase();
                String tagDescription = singleTag[1].toLowerCase();

                for(int i = 0; i < photoAlbum.albums.size(); i++){
                    Album currentAlb = photoAlbum.albums.get(i);

                    for(int j = 0; j < currentAlb.photoArrayList.size(); j++){
                        Photo currentPhoto = currentAlb.photoArrayList.get(j);

                        for(int k = 0; k < currentPhoto.tagArrayList.size(); k++){
                            Tag currentTag = currentPhoto.tagArrayList.get(k);

                            if(currentTag.getType().toLowerCase().equals(tagType)){
                                if(currentTag.getName().toLowerCase().contains(tagDescription)){
                                    searchList.add(currentPhoto);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            else if(parts.length == 3){
                String[] firstTag = parts[0].split("=");
                String[] secondTag = parts[2].split("=");

                String firstTagType = firstTag[0].toLowerCase();
                String firstTagDescription = firstTag[1].toLowerCase();
                String secondTagType = secondTag[0].toLowerCase();
                String secondTagDescription = secondTag[1].toLowerCase();

                if(parts[1].toLowerCase().equals("and")){
                    boolean firstTagAdded = false;
                    boolean secondTagAdded = false;

                    for(int i = 0; i < photoAlbum.albums.size(); i++){
                        Album currentAlb = photoAlbum.albums.get(i);

                        for(int j = 0; j < currentAlb.photoArrayList.size(); j++){
                            Photo currentPhoto = currentAlb.photoArrayList.get(j);

                            for(int k = 0; k < currentPhoto.tagArrayList.size(); k++){
                                Tag currentTag = currentPhoto.tagArrayList.get(k);

                                if(!firstTagAdded){
                                    if(currentTag.getType().toLowerCase().equals(firstTagType)){
                                        if(currentTag.getName().toLowerCase().contains(firstTagDescription)){
                                            firstTagAdded = true;
                                        }
                                    }
                                }
                                if(!secondTagAdded){
                                    if(currentTag.getType().toLowerCase().equals(secondTagType)){
                                        if(currentTag.getName().toLowerCase().contains(secondTagDescription)){
                                            secondTagAdded = true;
                                        }
                                    }
                                }
                                if(secondTagAdded && firstTagAdded){
                                    searchList.add(currentPhoto);
                                    break;
                                }
                            }
                        }
                    }
                }
                else if(parts[1].toLowerCase().equals("or")){
                    for(int i = 0; i < photoAlbum.albums.size(); i++){
                        Album currentAlb = photoAlbum.albums.get(i);

                        for(int j = 0; j < currentAlb.photoArrayList.size(); j++){
                            Photo currentPhoto = currentAlb.photoArrayList.get(j);

                            for(int k = 0; k < currentPhoto.tagArrayList.size(); k++){
                                Tag currentTag = currentPhoto.tagArrayList.get(k);

                                if(currentTag.getType().toLowerCase().equals(firstTagType)){
                                    if(currentTag.getName().toLowerCase().contains(firstTagDescription)){
                                        searchList.add(currentPhoto);
                                        break;
                                    }
                                }
                                else if(currentTag.getType().toLowerCase().equals(secondTagType)){
                                    if(currentTag.getName().toLowerCase().contains(secondTagDescription)){
                                        searchList.add(currentPhoto);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        catch(ArrayIndexOutOfBoundsException e){
            IncorrectParseDialog id = new IncorrectParseDialog();
            id.show(getSupportFragmentManager(), "Wrong Parse");
            e.printStackTrace();
            return;
        }

        Intent searchIntent = new Intent(this, Search.class);
        startActivity(searchIntent);
    }
}
