package com.example.Photo_App.Dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.example.Photo_App.MainActivity;
import com.example.Photo_App.R;

public class EditAlbumDialog extends AppCompatDialogFragment {

    private EditText albumName;
    public AlbumDialogListener listener;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final int pos = getArguments().getInt(MainActivity.BUNDLE_POS);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.edit_album, null);
        albumName = view.findViewById(R.id.edit_name);
        builder.setView(view)
                .setTitle("Edit Album Name")
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        return;
                    }
                })
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String name = albumName.getText().toString().trim();
                        listener.editAlbumName(name, pos);
                    }
                });
        return builder.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (AlbumDialogListener)context;
    }

    public interface AlbumDialogListener{
        void editAlbumName(String name, int pos);
    }
}
