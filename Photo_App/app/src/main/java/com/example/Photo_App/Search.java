package com.example.Photo_App;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.example.Photo_App.objects.Photo;

import java.util.ArrayList;

public class Search extends AppCompatActivity {

    public static final String RESULTS_PHOTO_POS="RESULTS_PHOTO_POS";

    private GridView searchGV;
    private ImageView image;
    private ArrayList<Photo> returnList;
    private PhotoAdapter photoAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        returnList =MainActivity.searchList;

        searchGV = findViewById(R.id.search_GV);

        photoAdapter = new PhotoAdapter();
        searchGV.setAdapter(photoAdapter);

        searchGV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ResultsView(position);
            }
        });
    }

    private void ResultsView(int pos){
        Bundle bund = new Bundle();
        bund.putInt(RESULTS_PHOTO_POS, pos);
        Intent resultsView = new Intent(this, ResultsView.class);
        resultsView.putExtras(bund);
        startActivity(resultsView);
    }

    private class PhotoAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return returnList.size();
        }

        @Override
        public Photo getItem(int position) {
            return returnList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = getLayoutInflater().inflate(R.layout.photo, parent, false);
            image = convertView.findViewById(R.id.photo);

            image.setImageBitmap(returnList.get(position).getBmap());
            return convertView;
        }
    }
}
